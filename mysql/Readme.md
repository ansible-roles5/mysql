# Mysql

## Présentation
---------------
* Ce rôle permet d'installer, créer une instance et piloter un serveur nodejs.

Il permet d'effectuer les actions suivantes:
- Installer les packages mysql
- configurer les fichiers de conf
- Créer des users et database
- Créer une table

## Prérequis
---------------

* Ce rôle a été testé sur: 
  - Ubuntu 18

## Variables du rôle
--------------
```yaml
---
# Packages mysql
mysql_packages:
  - mysql-client
  - mysql-common
  - mysql-server
  - python-mysqldb

# Mysql path conf  
mysql_path_conf: "/etc/mysql/mysql.conf.d"

# Mysql bind address
mysql_bind_address: '0.0.0.0'

# Mysql port
mysql_port: 3306
# Mysql db name
mysql_db_name: demo

# Mysql host ip
mysql_host_ip: "localhost"

# Mysql user
mysql_user: demo

# Mysql user password
mysql_user_password: demo

# Mysql databases
mysql_databases:
  - name: "{{ mysql_db_name }}"
    collation: utf8_general_ci
    encoding: utf8

# Mysql users
mysql_users:
  - name: "{{ mysql_user }}"
    host: '%'
    password: "{{ mysql_user_password }}"
    priv: '*.*:ALL'
```

## Dépendances
------------
RAS


## Exemple de playbook
----------------------

```yaml
    - hosts: mysql
      roles:
        - mysql
```
Il faut surcharger toutes les variables dans le repo d'inventaires avec des valeurs propres au projet en question.

## License
----------


## Information sur l'auteur
---------------------------
  - Aissam OURHOU